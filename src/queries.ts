import fetch from 'node-fetch';

/**
 * CONSTANTS
 */
const ethNode = 'http://localhost:8545';

const carolUrl = 'http://localhost:8005';
const daveUrl = 'http://localhost:8006';

const carolConnextChannel = '0x66920C67620b492C3FF7f904af6DC3a8B58D7C9C';
const daveConnextChannel = '0x7E513218D56ef4465208d587e9eff56e9035cd02';

const daveSignerAddress = '0x1b1968cAfFB691191CC05164d250a4bEF4aFaA65';

const carolPublicIdentifier =
  'vector8ZaxNSdUM83kLXJSsmj5jrcq17CpZUwBirmboaNPtQMEXjVNrL';
const davePublicIdentifier =
  'vector7mAydt3S3dDPWJMYSHZPdRo16Pru145qTNQYFoS8TrpXWW8HAj';
const rogerSignerAddress = '0x9CD130fE9aC1EF5173eEBE0C163C7a19f3E4ed2B';
// const chainId = 1337;

const connextPublicIdentifier =
  'vector8Uz1BdpA9hV5uTm6QUv5jj1PsUyCH8m8ciA94voCzsxVmrBRor';

/** sugarDaddy is the default wallet containing funds */
const sugarDaddy = '0x627306090abaB3A6e1400e9345bC60c78a8BEf57';

// const limitTimeout = "86400"
const highTimeout = '186400';

const preImage =
  '0x95e02c8389826c723217d30761cfac3e7bd76e6e4c2aa523213a6ddc67029e95';
const cancelPreImage =
  '0x0000000000000000000000000000000000000000000000000000000000000000';

/**
 * END OF CONSTANTS
 */

export const get = async ({ url }: { url: string }) =>
  (await fetch(url)).json();

const post = async ({ url, body }: { url: string; body: any }) => {
  const resp = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body)
  });
  return resp.json();
};

const createUserNode = async ({ name, url }: { name: string; url: string }) => {
  console.log(`Create ${name}'s NODE`);
  return post({
    url,
    body: {
      index: 0
    }
  });
};

export const createCarolNode = async (): Promise<any> =>
  createUserNode({ name: 'Carol', url: `${carolUrl}/node` });

export const createDaveNode = async (): Promise<any> =>
  createUserNode({ name: 'Dave', url: `${daveUrl}/node` });

const setupUserConnextChannel = async ({
  name,
  url,
  userIdentifier,
  chainId = 1337,
  timeout = highTimeout
}: {
  name: string;
  url: string;
  userIdentifier: string;
  chainId: number;
  timeout?: string | number;
}): Promise<any> => {
  console.log(`Setting up ${name} -> Connext channel`);
  return post({
    url,
    body: {
      counterpartyIdentifier: connextPublicIdentifier,
      publicIdentifier: userIdentifier,
      chainId,
      timeout
    }
  });
};

export const setupCarolConnextChannel = ({
  chainId,
  timeout
}: {
  chainId: number;
  timeout?: number | string;
}): Promise<any> =>
  setupUserConnextChannel({
    name: 'Carol',
    url: `${carolUrl}/setup`,
    chainId,
    userIdentifier: carolPublicIdentifier,
    timeout
  });

export const setupDaveConnextChannel = ({
  chainId,
  timeout
}: {
  chainId: number;
  timeout?: number | string;
}): Promise<any> =>
  setupUserConnextChannel({
    name: 'Dave',
    url: `${daveUrl}/setup`,
    chainId,
    userIdentifier: davePublicIdentifier,
    timeout
  });

export const sendToUserConnextChannel = async ({
  name,
  value = '0xDE0B6B3A7640000'
}: {
  name: string;
  value?: string;
}): Promise<any> => {
  console.log(
    `Send a transaction to ${name}->connext for ${parseInt(value)} Wei`
  );
  return post({
    url: ethNode,
    body: {
      jsonrpc: '2.0',
      method: 'eth_sendTransaction',
      params: [
        {
          from: sugarDaddy,
          to: carolConnextChannel,
          value
          // data: "0x",
        }
      ],
      id: 1
    }
  });
};

export const sendToConnext = async ({
  address = rogerSignerAddress,
  value = '0xDE0B6B3A7640000'
}: {
  address?: string;
  value?: string;
} = {}): Promise<any> => {
  console.log(`Send a transaction to "Connext" for ${parseInt(value)} Wei`);
  return post({
    url: ethNode,
    body: {
      jsonrpc: '2.0',
      method: 'eth_sendTransaction',
      params: [
        {
          from: sugarDaddy,
          to: address,
          value
        }
      ],
      id: 1
    }
  });
};

export const depositOnCarolConnextChannel = async ({
  assetId = '0x0000000000000000000000000000000000000000'
}: {
  assetId?: string;
} = {}): Promise<any> => {
  console.log("Making a deposit to Carol's account");
  return post({
    url: `${carolUrl}/deposit`,
    body: {
      channelAddress: carolConnextChannel,
      assetId,
      publicIdentifier: carolPublicIdentifier
    }
  });
};

const createTransfer = async ({
  url,
  publicIdentifier,
  channelAddress,
  recipient,
  assetId,
  amount
}: {
  url: string;
  publicIdentifier: string;
  channelAddress: string;
  recipient: string;
  assetId: string;
  amount: string;
}): Promise<any> => {
  const lockHash =
    '0x7783846bebe64b2dbf16ca981d27a73ada4bc6976e5be3ef92f0057af8368ce1';
  return post({
    url,
    body: {
      type: 'HashlockTransfer',
      publicIdentifier,
      channelAddress,
      amount,
      assetId,
      details: {
        lockHash,
        expiry: '0'
      },
      recipient,
      timeout: '48000'
    }
  });
};

export const createTransferFromCarolToDave = async ({
  amount,
  assetId = '0x0000000000000000000000000000000000000000'
}: {
  amount: string;
  assetId?: string;
}): Promise<any> => {
  console.info(
    `Transfering ${amount} from ${carolPublicIdentifier} -> ${carolConnextChannel} -> ${davePublicIdentifier}`
  );
  return createTransfer({
    url: `${carolUrl}/transfers/create`,
    publicIdentifier: carolPublicIdentifier,
    channelAddress: carolConnextChannel,
    recipient: davePublicIdentifier,
    assetId,
    amount
  });
};

const getBalance = async (address: string) => {
  return post({
    url: ethNode,
    body: {
      jsonrpc: '2.0',
      method: 'eth_getBalance',
      params: [address, 'latest'],
      id: 1
    }
  });
};

export const showDaveAddressBalance = async () => {
  const balance = await getBalance(daveSignerAddress);
  console.info("Dave's address balance:", balance, parseInt(balance.result));
};

export const showDaveConnextBalance = async () => {
  const balance = await getBalance(daveConnextChannel);
  console.info('Dave->Connext balance:', balance, parseInt(balance.result));
};

export const withDrawToDaveAddress = ({
  amount,
  assetId = '0x0000000000000000000000000000000000000000',
  fee = '0'
}: {
  amount: string;
  assetId?: string;
  fee?: string;
}): Promise<any> => {
  return post({
    url: `${daveUrl}/withdraw`,
    body: {
      publicIdentifier: davePublicIdentifier,
      channelAddress: daveConnextChannel,
      amount,
      assetId,
      recipient: daveSignerAddress,
      fee,
      meta: {
        hello: 'world'
      }
    }
  });
};

export const findTransferAndResolve = async ({
  routingId,
  tries
}: // transferId
{
  routingId: string;
  tries: number;
  // transferId: string;
}): Promise<boolean> => {
  console.log('# --------------------------------------------------- #');
  console.log("Resolving Carol's transfer to Dave");
  const routedTransfer = await get({
    url: `${daveUrl}/${davePublicIdentifier}/channels/${daveConnextChannel}/transfers/routing-id/${routingId}`
  });
  if (!routedTransfer) {
    console.log(`Dave has No transfer to resolve on this route`, routingId);
    console.log(`Retrying in 1000 ms ...`);
    tries++;
    return false;
  }

  const { transferId } = routedTransfer;
  console.log('Resolving transfer: ', transferId);
  await post({
    url: `${daveUrl}/transfers/resolve`,
    body: {
      publicIdentifier: davePublicIdentifier,
      channelAddress: daveConnextChannel,
      transferId,
      transferResolver: {
        preImage: preImage
      }
    }
  });
  return true;
};
