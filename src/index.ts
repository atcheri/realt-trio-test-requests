import { exit } from 'process';
import {
  createCarolNode,
  createDaveNode,
  createTransferFromCarolToDave,
  depositOnCarolConnextChannel,
  findTransferAndResolve,
  get,
  sendToConnext,
  sendToUserConnextChannel,
  setupCarolConnextChannel,
  setupDaveConnextChannel,
  showDaveAddressBalance,
  showDaveConnextBalance,
  withDrawToDaveAddress
} from './queries';

const nodeUrl = 'http://localhost:8007';

const run = async () => {
  try {
    console.info('---- Start Executing run function ----');

    const config = get({ url: `${nodeUrl}/config` });
    if (!config) {
      console.error('THE TRIO SERVER IS NOT RUNNING');
      exit(1);
    }

    console.info("---- 1) Creating Carol and Dave's nodes ----");
    await createCarolNode();

    await createDaveNode();

    console.info(
      "---- 2) Setting Carol and Dave's channels to Connext (aka. Roger) ----"
    );
    await setupCarolConnextChannel({
      chainId: 1337
    });
    await setupDaveConnextChannel({
      chainId: 1337
    });

    console.info(
      "---- 3) Sending some eth to the off-chain Carol's->Connext channel ----"
    );
    await sendToUserConnextChannel({ name: 'Carol' });

    console.info('---- 4) Depositing  ----');
    console.info(
      "---- 4.1) Sending an Eth deposit to Carol's channel onchain  ----"
    );
    await sendToConnext(); // doesn't need to be executed everytime according to Sebastien Kraft.

    console.info(
      "---- 4.2) Adding the Eth to Carol`'s offchain balance (need to wait for the tx to be mined and then call: ...) ----"
    );
    await depositOnCarolConnextChannel();

    console.info(
      '---- 5) Creating a transfer from Carol -> Dave (through Connext) ----'
    );
    const amount = '37000000000000';
    const { routingId } = await createTransferFromCarolToDave({ amount });

    console.info(
      `---- 6) Resolving the transfer going through routingId ${routingId} ----`
    );
    showDaveConnextBalance()
      .then(() => {
        const tries = 0;
        const maxTries = 10;
        return new Promise<void>((resolve, reject) => {
          const intervalId = setInterval(async () => {
            console.info('Trying to resolve');
            const resolved = await findTransferAndResolve({ routingId, tries });
            if (tries > maxTries) {
              clearInterval(intervalId);
              return reject(`Max tries (${maxTries}) reached`);
            }
            if (resolved) {
              clearInterval(intervalId);
              return resolve();
            }
          }, 1000);
        });
      })
      .then(showDaveConnextBalance)
      .then(async () => {
        await withDrawToDaveAddress({ amount });
      })
      .then(showDaveConnextBalance)
      .then(showDaveAddressBalance)
      .catch(console.error)
      .finally(() => {
        console.info('---- End Executing run function ----');
      });
  } catch (err) {
    const detail = JSON.stringify(err, null, 2);
    console.error(`Error on run: ${detail}`);
  }
};

run();

// (() => {
//   showDaveAddressBalance();
// })();
